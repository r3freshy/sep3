package alignment;

import nucleotide.ListNucleotideSequence;
import nucleotide.NucleotideSequence;
import nucleotide.PairNucleotideSequence;

public abstract class Alignment {
	protected ListNucleotideSequence lns;
	protected PairNucleotideSequence pns;
	Alignment(NucleotideSequence ns1, NucleotideSequence ns2){
		this.pns = new PairNucleotideSequence(ns1,ns2);
	}
	Alignment(ListNucleotideSequence lns){
		this.lns = lns;
	}
	Alignment(PairNucleotideSequence pns){
		this.pns = pns;
	}
	abstract NucleotideSequence[] toArray();
	abstract void dpMethod();
	abstract void proposedMethod();
}
