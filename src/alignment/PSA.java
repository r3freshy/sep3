package alignment;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.sat4j.maxsat.SolverFactory;
import org.sat4j.maxsat.WeightedMaxSatDecorator;
import org.sat4j.maxsat.reader.WDimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IOptimizationProblem;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.OptToSatAdapter;

import cnfdimacs.DIMACS;
import nucleotide.NucleotideSequence;
import nucleotide.PairNucleotideSequence;

public class PSA extends Alignment{
	int M[][];
	int X[][];
	int length[];
	ArrayList <Integer[]>g;
	ArrayList <Integer[]>og;
	ArrayList <Integer[]> eg;
	int match_weight;
	int mismatch_weight;
	int gap_weight;
	int opengap_weight;
	int extendgap_weight;
	int varcount;
	
	int total_weight;
	DIMACS dimacs;
	
	
	PSA(PairNucleotideSequence pns){
		super(pns);
		dimacs = new DIMACS();
		setupConstraints();
		setupWeights(5,-1,-1,-4,-4);
	}
	PSA(PairNucleotideSequence pns,int mw, int mmw, int gw, int ogw, int egw){
		super(pns);
		dimacs = new DIMACS();
		setupConstraints();
		setupWeights(mw, mmw, gw, ogw, egw);
	}
	PSA(NucleotideSequence ns1,NucleotideSequence ns2){
		super(ns1,ns2);
		dimacs = new DIMACS();
		setupConstraints();
		setupWeights(5,-1,-1,-4,-4);
	}
	PSA(NucleotideSequence ns1,NucleotideSequence ns2,int mw, int mmw, int gw, int ogw, int egw){
		super(ns1,ns2);
		dimacs = new DIMACS();
		setupConstraints();
		setupWeights(mw, mmw, gw, ogw, egw);
	}
	
	private void setupConstraints(){
		varcount = 1;
		length = new int[2];
		g = new ArrayList<Integer[]>();
		og = new ArrayList<Integer[]>();
		eg = new ArrayList<Integer[]>();
		length[0] = pns.getFirst().sizeS();
		length[1] = pns.getSecond().sizeS();
		M = new int[length[0]][length[1]];
		X = new int[length[0]][length[1]];
		g.add( new Integer[length[0]] );
		g.add( new Integer[length[1]] );
		og.add(new Integer[length[0]] );
		og.add(new Integer[length[1]] );
		eg.add(new Integer[length[0]] );
		eg.add(new Integer[length[1]] );
	}
	
	private void setupWeights(int mw, int mmw, int gw, int ogw, int egw){
		match_weight = mw;
		mismatch_weight = mmw;
		gap_weight = gw;
		opengap_weight = ogw;
		extendgap_weight = egw;
	}
	
	private void matchedAndMismatchedSet(){
		dimacs.comment("matchedAndMismatchedSet");
		for(int i = 0; i<length[0] ;i++){
			for(int j = 0 ; j<length[1] ;j++){
				if(pns.checkMatch(i, j)){
					if(match_weight>0){
						total_weight += match_weight;
					}
					else{
						total_weight -= match_weight;
					}
					M[i][j] = this.varcount;
					dimacs.clause(varcount++,match_weight);
				}
				else{
					if(mismatch_weight>0){
						total_weight += mismatch_weight;
					}
					else{
						total_weight -= mismatch_weight;
					}
					X[i][j] = this.varcount;
					dimacs.clause(varcount++,mismatch_weight);
				}
			}
		}
//		System.out.println("M");
//		for(int i = 0 ;i<length[0];i++){
//			System.out.println(Arrays.toString(M[i]));
//		}
//		System.out.println("X");
//		for(int i = 0 ;i<length[0];i++){
//			System.out.println(Arrays.toString(X[i]));
//		}
	}
	
	private void gapSet(){
		dimacs.comment("g[0]");
		for(int i = 0 ; i<length[0] ; i++){
			if(gap_weight>0){
				total_weight += gap_weight;
			}
			else{
				total_weight -= gap_weight;
			}
			g.get(0)[i] = this.varcount;
			dimacs.clause(varcount++,gap_weight);
		}
		dimacs.comment("g[1]");
		for(int i = 0 ; i<length[1] ; i++){
			if(gap_weight>0){
				total_weight += gap_weight;
			}
			else{
				total_weight -= gap_weight;
			}
			g.get(1)[i] = this.varcount;
			dimacs.clause(varcount++,gap_weight);
		}
		dimacs.comment("og[0]");
		for(int i = 0 ; i<length[0] ; i++){
			if(opengap_weight>0){
				total_weight += opengap_weight;
			}
			else{
				total_weight -= opengap_weight;
			}
			og.get(0)[i] = this.varcount;
			dimacs.clause(varcount++,opengap_weight);
		}
		dimacs.comment("og[1]");
		for(int i = 0 ; i<length[1] ; i++){
			if(opengap_weight>0){
				total_weight += opengap_weight;
			}
			else{
				total_weight -= opengap_weight;
			}
			og.get(1)[i] = this.varcount;
			dimacs.clause(varcount++,opengap_weight);
		}
		dimacs.comment("eg[0]");
		for(int i = 0 ; i<length[0] ; i++){
			if(extendgap_weight>0){
				total_weight += extendgap_weight;
			}
			else{
				total_weight -= extendgap_weight;
			}
			eg.get(0)[i] = this.varcount;
			dimacs.clause(varcount++,extendgap_weight);
		}
		dimacs.comment("eg[1]");
		for(int i = 0 ; i<length[1] ; i++){
			if(extendgap_weight>0){
				total_weight += extendgap_weight;
			}
			else{
				total_weight -= extendgap_weight;
			}
			eg.get(1)[i] = this.varcount;
			dimacs.clause(varcount++,extendgap_weight);
		}
		
	}
	
	private void noRepeatAtS1(){
		total_weight = 999999999;
		dimacs.comment("norepeats1");
		ArrayList<Integer> al = new ArrayList<Integer>();
		for( int i = 0; i < length[0]; ++i ) {
			al.clear();
			al.add(g.get(0)[i]);
			for( int j = 0 ; j < length[1]; ++ j ) {
				if( M[i][j] != 0 ) al.add( M[i][j] );
				if( X[i][j] != 0 ) al.add( X[i][j] );
			}
			dimacs.norepeat( al , total_weight );
			dimacs.clause(al, total_weight);
    
		}
	}
	private void noRepeatAtS2(){
		dimacs.comment("norepeats2");
		ArrayList<Integer> al = new ArrayList<Integer>();
		for( int j = 0 ; j < length[1]; ++ j ) {
			al.clear();
			al.add( g.get(1)[j] );
		    for( int i = 0; i < length[0]; ++i ) {
		      if( M[i][j] != 0 ) al.add( M[i][j] );
		      if( X[i][j] != 0 ) al.add( X[i][j] );
		    }
		    dimacs.norepeat( al , total_weight );
			dimacs.clause(al, total_weight);
		    
		  }
	}
	
	private void nocross(){
		dimacs.comment("no cross from right to left");
		  
		for (int i = 0; i < length[0]; ++i) {
			for (int j = 0; j < length[1]; ++j)  {
				int maxm = length[0];
				int minm = 0; 
				for (int a = i + 1; a < maxm; ++a){
					for (int b = j - 1; b >= minm; --b){
						ArrayList<Integer> al = new ArrayList<Integer>();
						if( M[i][j] != 0 && M[a][b] != 0 ) {
							al.add(-M[i][j]); al.add(-M[a][b]);
							dimacs.clause(al, total_weight);
						}
						al.clear();
						if( X[i][j] != 0 && X[a][b] != 0 ) {
							al.add(-X[i][j]); al.add(-X[a][b]);
							dimacs.clause(al, total_weight);
						}
						al.clear();
						if( M[i][j] != 0 && X[a][b] != 0) {
							al.add(-M[i][j]); al.add(-X[a][b]);
							dimacs.clause(al, total_weight);
						}
						al.clear();
						if( X[i][j] != 0 && M[a][b] != 0 ) {
							al.add(-X[i][j]); al.add(-M[a][b]);
							dimacs.clause(al, total_weight);
						}
					}
				}
			}
		}
		dimacs.comment("no cross from left to right");
		for (int i = 0; i < length[0]; ++i) {
			for (int j = 0; j < length[1]; ++j){

		    int maxn = length[1];
		    int minn = 0;
		      
		    for (int a = i - 1; a >= minn; --a){
		    	for (int b = j + 1; b < maxn; ++b){
		    		ArrayList<Integer> al = new ArrayList<Integer>();
		    		if( M[i][j] != 0 && M[a][b] != 0 ) {
		    			al.add(-M[i][j]); al.add(-M[a][b]);
		    			dimacs.clause(al, total_weight);
		    		}
		    		al.clear();
		    		if( X[i][j] != 0 && X[a][b] != 0 ) {
		    			al.add(-X[i][j]); al.add(-X[a][b]);
		    			dimacs.clause(al, total_weight);
		    		}
		    		al.clear();
		    		if( M[i][j] != 0 && X[a][b] != 0 ) {
		    			al.add(-M[i][j]); al.add(-X[a][b]);
		    			dimacs.clause(al, total_weight);
		    		}
		    		al.clear();
		    		if( X[i][j] != 0 && M[a][b] != 0 ) {
		    			al.add(-X[i][j]); al.add(-M[a][b]);
		    			dimacs.clause(al, total_weight);
		    		}
		    	} 
		    }
			}
		}
	}
	
	private void checkOpenAndExtendGap(){

		  dimacs.comment("opening a gap");
		  
		  dimacs.equal( g.get(0)[0], og.get(0)[0], total_weight  ); // first position is the same (there is no position -1)
		  for( int i = 1 ; i < length[0]; ++ i ) {
			  dimacs.and( og.get(0)[i], -g.get(0)[i-1], g.get(0)[i] , total_weight);
		  }
		  dimacs.equal( g.get(1)[0], og.get(1)[0], total_weight  );
		  //EQUAL( G[1][0], OpenGap[1][0], total_weight  ); // first position is the same (there is no position -1)
		  for( int j = 1 ; j < length[1]; ++ j ) {
			  dimacs.and( og.get(1)[j], -g.get(1)[j-1], g.get(1)[j] , total_weight);
			  //dimacs.AND( OpenGap[1][j], -G[1][j-1], G[1][j] , total_weight );
		  }
		  
		  dimacs.comment("extending a gap");
		  //formula << "c extending a gap " << endl;
		  dimacs.clause(- eg.get(0)[0], total_weight);
		  //UNIT( - ExtendGap[0][0], formula  ); // first position cannot be extend gap
		  for( int i = 1 ; i < length[0]; ++ i ) {
			  dimacs.and( eg.get(0)[i], g.get(0)[i-1], g.get(0)[i] , total_weight );
		  }
		  dimacs.clause(- eg.get(1)[0], total_weight);
		  //UNIT( - ExtendGap[1][0], formula  ); // first position cannot be extend gap
		  for( int j = 1 ; j < length[1]; ++ j ) {
			  dimacs.and( eg.get(1)[j], g.get(1)[j-1], g.get(1)[j] , total_weight );
		  }
	}
	
	public void decode(){
		WeightedMaxSatDecorator maxsat = new WeightedMaxSatDecorator(SolverFactory.newLight());
		
        WDimacsReader reader = new WDimacsReader(maxsat);
        int [] model;
        int [][]MXdecode = new int[length[0]][length[1]];
        int [] g1decode = new int [length[0]];
        int [] g2decode = new int [length[1]];
        int [] og1decode = new int [length[0]];
        int [] og2decode = new int [length[1]];
        int [] eg1decode = new int [length[0]];
        int [] eg2decode = new int [length[1]];
        
        try {
        	
        	IOptimizationProblem problem = (IOptimizationProblem)reader.parseInstance("a"+".wcnf");
        	
			IProblem satproblem = new OptToSatAdapter(problem);
			
			//maxsat.setTimeoutMs(100);
            if (satproblem.isSatisfiable()) {
            	model = satproblem.model();
                
                int x = 0;
            	int y = 0;
            	int cc = 0;
            	System.out.println("MX");
            	for(int i =0 ;i<length[0];i++){
            		for(int j = 0 ; j<length[1];j++){
            			if(model[cc] > 0)
            				MXdecode[i][j] = model[cc++];
            			
            			else
            				MXdecode[i][j] = model[cc++];
            		}
            		System.out.println(Arrays.toString(MXdecode[i]));
            	}
            	for(int i = 0 ;i<length[0];i++){
            		g1decode[i] = model[cc++];
            	}
            	for(int i = 0 ;i<length[1];i++){
            		g2decode[i] = model[cc++];
            	}
            	for(int i = 0 ;i<length[0];i++){
            		og1decode[i] = model[cc++];
            	}
            	for(int i = 0 ;i<length[1];i++){
            		og2decode[i] = model[cc++];
            	}
            	for(int i = 0 ;i<length[0];i++){
            		eg1decode[i] = model[cc++];
            	}
            	for(int i = 0 ;i<length[1];i++){
            		eg2decode[i] = model[cc++];
            	}
            	System.out.println("G1");
            	System.out.println(Arrays.toString(g1decode));
            	System.out.println("G2");
            	System.out.println(Arrays.toString(g2decode));
            	System.out.println("OG1");
            	System.out.println(Arrays.toString(og1decode));
            	System.out.println("OG2");
            	System.out.println(Arrays.toString(og2decode));
            	System.out.println("EG1");
            	System.out.println(Arrays.toString(eg1decode));
            	System.out.println("EG2");
            	System.out.println(Arrays.toString(eg2decode));
                
//                for(int i = 0 ; i<decode.length;i++){
//                	for(int j = 0 ; j<decode[0].length;j++){
//                		System.out.print(decode[i][j] + " ");
//                	}
//                	System.out.println();
//                }
            } else {
                System.out.println("Unsatisfiable !");
            }
        } catch (FileNotFoundException e) {
        	System.out.println("asdas");
            // TODO Auto-generated catch block
        } catch (ParseFormatException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (ContradictionException e) {
            System.out.println("Unsatisfiable (trivial)!");
        } catch (TimeoutException e) {
            System.out.println("Timeout, sorry!");      
        }
	}

	@Override
	void dpMethod() {
		
	}

	@Override
	void proposedMethod() {
		long startTime, total = 0;
		startTime = System.currentTimeMillis();
		matchedAndMismatchedSet();
		System.out.println("MMS");
		gapSet();
		System.out.println("GS");
		noRepeatAtS1();
		System.out.println("NRS1");
		noRepeatAtS2();
		System.out.println("NRS2");
		nocross();
		System.out.println("NC");
		checkOpenAndExtendGap();
		System.out.println("COAEG");
		dimacs.problemLine(varcount);
		dimacs.writeDimacs("a.wcnf");
		total = System.currentTimeMillis() - startTime;
        System.out.println(total);
        startTime = System.currentTimeMillis();
        decode();
        total = System.currentTimeMillis() - startTime;
        System.out.println(total);
	}

	@Override
	NucleotideSequence[] toArray() {
		return pns.toArray();
	}
	
	public static void main(String[] args) {
		NucleotideSequence s1 = new NucleotideSequence("AATAATAAT");
		NucleotideSequence s2 = new NucleotideSequence("ATAATAAT");
		PSA a = new PSA(s1,s2);
		a.proposedMethod();
		//System.out.println(a.dimacs);
	}
	
	
	
	
}
