package nucleotide;

import java.util.Arrays;

public class PairNucleotideSequence {
	private NucleotideSequence[] ps;
	public PairNucleotideSequence() {
	}
	
	public PairNucleotideSequence(NucleotideSequence[] ns){
		if(ns.length==2){
			ps = ns;
		}
		else{
			System.err.print("ERROR");
		}
	}
	
	public boolean checkMatch(int i, int j){
		return ps[0].getCharS(i) == ps[1].getCharS(j);
	}
	
	public PairNucleotideSequence(NucleotideSequence s1, NucleotideSequence s2){
		ps = new NucleotideSequence[2];
		ps[0] = s1;
		ps[1] = s2;
	}
	public void setFirst(NucleotideSequence s){
	
		ps[0] = s;
	
	}
	public void setSecond(NucleotideSequence s){
		
		ps[1] = s;
	
	}
	public void setPair(NucleotideSequence[] ns){
		if(ns.length==2){
			ps = ns;
		}
		else{
			System.err.print("ERROR");
		}
	}
	public void setPair(NucleotideSequence s1, NucleotideSequence s2){
		if(ps==null) ps = new NucleotideSequence[2];
		ps[0] = s1;
		ps[1] = s2;
	}
	public NucleotideSequence getFirst(){
		return ps[0];
	}
	
	public NucleotideSequence getSecond(){
		return ps[1];
	}
	public NucleotideSequence get(int index){
		if(index>=2) return null;
		return ps[index];
	}
	
	public NucleotideSequence[] toArray(){
		return ps;
	}
	
	public boolean isEmpty(){
		return (ps==null);
	}
	
	public void clear(){
		ps = null;
	}
	
	public String toString(){
		return Arrays.toString(ps);
	}
	
//	public static void main(String[] args) {
//		NucleotideSequence ns1 = new NucleotideSequence("ATCG");
//		NucleotideSequence ns2 = new NucleotideSequence("ATTT");
//		
//		PairNucleotideSequence p = new PairNucleotideSequence(ns1,ns2);
//		PairNucleotideSequence p1 = new PairNucleotideSequence();
//		System.out.println(p1.isEmpty());
//		System.out.println(p.isEmpty());
//		System.out.println(p.get(0));
//		p.clear();
//		System.out.println(p);
//		
//	}
	
	
}
