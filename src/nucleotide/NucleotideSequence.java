package nucleotide;

public class NucleotideSequence {
	
	private String s;
	private String as;
	
	public NucleotideSequence(String s) {
		this.s = s;
	}
	
	public char getCharS(int index){
		return s.charAt(index);
	}
	
	public void setSequence(String s){
		this.s = s;
	}
	
	public void setAlignSequence(String as){
		this.as = as;
	}
	
	public String getSequence(){
		return s;
	}
	
	public String getAlignSequence(){
		return as;
	}
	
	public int sizeS(){
		return s.length();
	}
	
	public int sizeAS(){
		return as.length();
	}
	
	public String toString(){
		return "Sequence : " +s +" | Aligned Sequence : " + as;
	}
	
}
