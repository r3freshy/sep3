package nucleotide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;

public class ListNucleotideSequence {
	private ArrayList<NucleotideSequence> list;
	
	//constructor
	public ListNucleotideSequence(NucleotideSequence[] ns){
		list = new ArrayList<NucleotideSequence>(Arrays.asList(ns));
	}
	public ListNucleotideSequence(Collection<NucleotideSequence> ns){
		list = new ArrayList<NucleotideSequence>(ns);
	}
	public ListNucleotideSequence(){
		list = new ArrayList<NucleotideSequence>();
	}
	
	//add 
	public void add(NucleotideSequence[] ns){
		list.addAll(Arrays.asList(ns));
	}
	public void add(Collection<NucleotideSequence> ns){
		list.addAll(ns);
	}
	public void add(NucleotideSequence ns){
		list.add(ns);
	}
	
	//remove
	public NucleotideSequence remove(int index){
		return list.remove(index);
	}
	
	public boolean remove(Object O){
		return list.remove(O);
	}
	
	//clear
	public void clear(){
		list.clear();
	}
	
	//clear
	public NucleotideSequence get(int index){
		return list.get(index);
	}
	
	public int size(){
		return list.size();
	}
	
	
	
	//isEmpty
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	
	public ArrayList<NucleotideSequence> toList(){
		return list;
	}
	
	public NucleotideSequence[] toArray(){
		return (NucleotideSequence[]) list.toArray();
	}
	
	public String toString(){
		return list.toString();
	}
	
	public static void main(String[] args) {
		NucleotideSequence[] ns1 = {new NucleotideSequence("A"), new NucleotideSequence("T")};
		NucleotideSequence[] ns2 = {new NucleotideSequence("C"), new NucleotideSequence("G")};
		ListNucleotideSequence lns = new ListNucleotideSequence(ns1);
		
		ArrayList<NucleotideSequence> test = new ArrayList<NucleotideSequence>();
		test.addAll(Arrays.asList(ns2));
		
		
		lns.add(test);
		
		System.out.println(lns);
		
		lns.remove(0);
		System.out.println(lns);
		
	}
	
}
