package cnfdimacs;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class CopyOfDIMACS {
	ArrayList<Integer> a1 = new ArrayList<Integer>();
	//ArrayList<Integer> a2 = new ArrayList<Integer>();
	String dimacs="";
	int clause=0;
	InputStream inputStream = null;
	OutputStream outputStream = null;
	public void comment(String c){
		
		dimacs += "c " + c +"\n";
	}
	public void problemLine(int var){
		
		dimacs = new StringBuilder(("p wcnf "+var + " "+ clause + "\n").length()+dimacs.length()).append(("p wcnf "+var + " "+ clause + "\n")).append(dimacs).toString();
	}
	
	public void clause(int literal,int weight){
		if(weight<0){
			literal = -literal;
			weight = - weight;
		}
		clause++;
		dimacs = new StringBuilder(dimacs.length()+(""+weight+ " "+ literal + " 0\n").length()).append(dimacs).append(""+weight+ " "+ literal + " 0\n").toString();
	} 
	public void clause(ArrayList<Integer> literals,int weight){
		String l = ""; String negative = "";
		if(weight<0){
			weight = (-weight);
			negative = "-";
		}
		for(int i=0;i<literals.size();i++){
			l += negative+(literals.get(i)) + " ";
		}
		dimacs = new StringBuilder(dimacs.length()+(""+weight+" ").length()+l.length()+"0\n".length()).append(dimacs).append(""+weight+" ").append(l).append("0\n").toString();
		clause++;
	}
	
	public void and( int x, int a, int b, int total_weight  )
	{
	  a1.clear();
	  a1.add(  x );
	  a1.add( -a );
	  a1.add( -b );
	  clause( a1, total_weight );
	  a1.clear();
	  a1.add( -x );
	  a1.add(  a );
	  clause( a1, total_weight );
	  a1.set(1, b);
	  clause( a1, total_weight );
	}
	
	public void equal( int x, int a, int total_weight )
	{
	  a1.clear();
	  a1.add( -x );
	  a1.add(  a );
	  clause(a1, total_weight);
	  a1.clear();
	  a1.add( x );
	  a1.add( -a );
	  clause( a1, total_weight );
	}
	
	public void norepeat( ArrayList<Integer> literals, int total_weight )
	{
		a1.clear();
		a1.add(0); a1.add(0);
		for( int i = 0 ; i + 1 < literals.size(); ++ i )
		{
			a1.set(0, -literals.get(i));
			for( int j = i+1; j < literals.size(); ++ j )
			{
				a1.set(1, -literals.get(j));
				clause( a1, total_weight );
			}
		}
	}
	public String toString(){
		return dimacs;
	}
	private void setInStream(){
		inputStream = new ByteArrayInputStream(dimacs.getBytes());
	}
	public void writeDimacs(String filename){
		try {
			setInStream();
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(new File(filename));
	 
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
	 
			System.out.println("Done!");
	 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	 
			}
		}
	}
}
