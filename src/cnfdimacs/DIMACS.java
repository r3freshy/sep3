package cnfdimacs;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DIMACS {
	ArrayList<Integer> a1 = new ArrayList<Integer>();
	//ArrayList<Integer> a2 = new ArrayList<Integer>();
	String dimacs="";
	int clause=0;
	InputStream inputStream = null;
	OutputStream outputStream = null;
	public void comment(String c){
		dimacs = dimacs.concat("c ").concat(c).concat("\n");
		//dimacs = dimacs + "c " + c + "\n";
	}
	public void problemLine(int var){
		dimacs = "p wcnf ".concat(""+var).concat(" ").concat(clause+"\n").concat(dimacs);
		//dimacs = "p wcnf " + var + " " + clause + "\n" + dimacs
	}
	
	public void clause(int literal,int weight){
		if(weight<0){
			literal = -literal;
			weight = - weight;
		}
		clause++;
		dimacs = dimacs.concat(weight+" ").concat(literal + " 0\n");
		//dimacs += weight+ " "+ literal + " 0\n";
	} 
	public void clause(ArrayList<Integer> literals,int weight){
		String l = ""; String negative = "";
		if(weight<0){
			weight = (-weight);
			negative = "-";
		}
		for(int i=0;i<literals.size();i++){
			l = l.concat(negative).concat(literals.get(i)+" ");
			//l += negative+(literals.get(i)) + " ";
		}
		clause++;
		dimacs = dimacs.concat(weight+" ").concat(l + " 0\n");
		//dimacs += weight+ " "+ l + "0\n";
		
	}
	
	public void and( int x, int a, int b, int total_weight  )
	{
	  a1.clear();
	  a1.add(  x );
	  a1.add( -a );
	  a1.add( -b );
	  clause( a1, total_weight );
	  a1.clear();
	  a1.add( -x );
	  a1.add(  a );
	  clause( a1, total_weight );
	  a1.set(1, b);
	  clause( a1, total_weight );
	}
	
	public void equal( int x, int a, int total_weight )
	{
	  a1.clear();
	  a1.add( -x );
	  a1.add(  a );
	  clause(a1, total_weight);
	  a1.clear();
	  a1.add( x );
	  a1.add( -a );
	  clause( a1, total_weight );
	}
	
	public void norepeat( ArrayList<Integer> literals, int total_weight )
	{
		a1.clear();
		a1.add(0); a1.add(0);
		for( int i = 0 ; i + 1 < literals.size(); ++ i )
		{
			a1.set(0, -literals.get(i));
			for( int j = i+1; j < literals.size(); ++ j )
			{
				a1.set(1, -literals.get(j));
				clause( a1, total_weight );
			}
		}
	}
	public String toString(){
		return dimacs;
	}
	private void setInStream(){
		inputStream = new ByteArrayInputStream(dimacs.getBytes());
	}
	public void writeDimacs(String filename){
		try {
			setInStream();
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(new File(filename));
	 
			int read = 0;
			byte[] bytes = new byte[1024];
	 
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
	 
			System.out.println("Done!");
	 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	 
			}
		}
	}
}
